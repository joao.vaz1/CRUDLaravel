<?php

use Illuminate\Support\Facades\Route;
use App\Models\Livros;
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('inicio');
});

Route::post('/cadastrar-livros', function(Request $request){
    
    Livros::create([
        'nome' => $request->nome,
        'preco' => $request->preco
       ]);
     
    echo "Livro salvo!";
});

Route::get('/ver-livros/{id}', function($id){
    //dd(Livros::find($id));
    $livros = Livros::find($id);
    return view('ver', ['livros' => $livros]);

});

Route::get('/editar-livros/{id}', function($id){
    //dd(Livros::find($id));
    $livros = Livros::find($id);
    return view('editar', ['livros' => $livros]);
});

Route::post('/editar-livros/{id}', function(Request $request, $id){
    $livros = Livros::find($id);
    $livros->update([
        'nome' => $request->nome,
        'preco' => $request->preco
       ]);
     
    echo "Livro alterado!";
    echo $livros;
});
