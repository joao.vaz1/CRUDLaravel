<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('livros', 'ApiController@getAllLivros');
Route::get('livros/{id}', 'ApiController@getLivro');
Route::post('livros', 'ApiController@createLivro');
Route::put('livros/{id}', 'ApiController@updateLivro');
Route::delete('livros/{id}','ApiController@deleteLivro');