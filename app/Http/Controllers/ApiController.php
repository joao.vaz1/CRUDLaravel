<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Livros;

class ApiController extends Controller
{
    public function getAllLivros() {
        $livros = Livros::get()->toJson(JSON_PRETTY_PRINT);
        return response($livros, 200);
    }
  
    public function createLivro(Request $request) {

        $livros = new Livros;
        $livros->nome = $request->nome;
        $livros->preco = $request->preco;
        $livros->save();

        return response()->json([
            "message" => "livro criado!"
        ], 201);

    }
  
      public function getLivro($nome) {
        if (Livros::where('nome', $nome)->exists()) {
            $livros = Livros::where('nome', $nome)->get()->toJson(JSON_PRETTY_PRINT);
            return response($livros, 200);
          } else {
            return response()->json([
              "message" => "livro não encontrado!"
            ], 404);
          }
      }
  
      public function updateLivro(Request $request, $id) {
        if (Livros::where('id', $id)->exists()) {
            $livros = Livros::find($id);
            $livros->nome = is_null($request->nome) ? $livros->nome : $request->nome;
            $livros->preco = is_null($request->preco) ? $livros->preco : $request->preco;
            $livros->save();
    
            return response()->json([
                "message" => "records updated successfully"
            ], 200);
            } else {
            return response()->json([
                "message" => "Livro não encontrado"
            ], 404);
            
        }
      }
  
      public function deleteLivro($id) {
        if(Livros::where('id', $id)->exists()) {
            $livros = Livros::find($id);
            $livros->delete();
    
            return response()->json([
              "message" => "records deleted"
            ], 202);
          } else {
            return response()->json([
              "message" => "Livro not found"
            ], 404);
          }
      }
}
