<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Livros</title>
        <link href="./sass/app.scss" rel="stylesheet" type="text/css">
    </head>
    <body>
        <form action="/cadastrar-livros" method="POST">
            @csrf
            <label id="a">Nome: </label>
            <input type="text" name="nome"></input>
            <label>Preço: </label>
            <input type="text" name="preco"></input>
            <button type="submit">Salvar livro</button>
        </form>
    </body>
</html>