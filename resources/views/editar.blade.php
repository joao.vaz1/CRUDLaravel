<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Livros</title>
    </head>
    <body>
        <form action="/editar-livros/{{ $livros->id }}" method="POST">
            @csrf
            <label>Nome: </label>
            <input type="text" name="nome" value="{{ $livros->nome }}"></input>
            <label>Preço: </label>
            <input type="text" name="preco" value="{{ $livros->preco }}"></input>
            <button type="submit">Salvar livro</button>
        </form>
    </body>
</html>